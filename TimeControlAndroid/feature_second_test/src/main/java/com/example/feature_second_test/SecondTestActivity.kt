package com.example.feature_second_test

import com.example.core_architecture.base_fragment.BaseActivity
import com.example.core_architecture.base_fragment.BaseFragment
import com.example.core_architecture.base_fragment.BaseRecyclerFragment

class SecondTestActivity : BaseActivity() {
    override val getFragment: BaseRecyclerFragment = SecondTestFragment()
}