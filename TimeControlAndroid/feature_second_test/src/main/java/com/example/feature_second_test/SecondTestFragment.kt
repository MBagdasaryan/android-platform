package com.example.feature_second_test

import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.core_architecture.base_fragment.BaseFragment
import com.example.core_architecture.base_fragment.BaseRecyclerFragment
import com.example.core_architecture.base_fragment.replace.ReplaceUtils
import com.example.core_ui.base_block.BaseBlockItem
import com.example.core_ui.base_block.baseBlock
import com.example.core_ui.base_button.BaseButtonItem
import com.example.core_ui.base_button.baseButton
import com.example.core_ui.simple_block.SimpleBlockItem
import com.example.core_ui.simple_block.simpleBlock

class SecondTestFragment : BaseRecyclerFragment() {
    override var BLOCK_SIZE = 1

    override fun initViews() {
        recyclerBlock[0] = simpleBlock(
            requireContext(),
            SimpleBlockItem(
                blockTitle = "Some block",
                dataSet = arrayOf(
                    baseButton(
                        requireContext(),
                        BaseButtonItem(
                            btnText = "Hello from Second activity",
                            setOnClickListener = {
                                Toast.makeText(requireContext(), "Clicked!!!", Toast.LENGTH_SHORT).show()
                            }
                        )
                    )
                )
            )
        )
    }
}
