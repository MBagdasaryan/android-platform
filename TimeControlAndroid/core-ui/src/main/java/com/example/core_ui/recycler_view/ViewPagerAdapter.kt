package com.example.core_architecture.base_fragment.recycler_view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.core_ui.R

class ViewPagerAdapter(
    private val dataSet: Array<ConstraintLayout>,
    private val callback: Callback
) :
    RecyclerView.Adapter<ViewPagerAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var mainLayout: ConstraintLayout = view.findViewById(R.id.view_pager_main_layout)

        init {
            if(adapterPosition != RecyclerView.NO_POSITION) {
                callback.setOnItemClicked()
            }
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.view_pager_adapter, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element

        if(dataSet[position].parent != null) {
            (dataSet[position].parent as ViewGroup).removeView(dataSet[position])
        }
        viewHolder.mainLayout.addView( dataSet[position])
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

    interface Callback {
        fun setOnItemClicked()
    }

}