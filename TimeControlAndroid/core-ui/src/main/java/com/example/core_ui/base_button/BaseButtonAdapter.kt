package com.example.core_ui.base_button

import android.content.Context

fun baseButton(
    context: Context,
    item: BaseButtonItem
): BaseButtonView {
    val view = BaseButtonView(context)
    view.setBaseButton(item)

    return view
}