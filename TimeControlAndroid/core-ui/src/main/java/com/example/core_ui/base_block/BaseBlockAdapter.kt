package com.example.core_ui.base_block

import android.content.Context

/** This block is intended for displaying RecyclerView */
fun baseBlock(
    context: Context,
    item: BaseBlockItem
): BaseBlockView {
    val view = BaseBlockView(context)
    view.setBaseBlock(item)

    return view
}