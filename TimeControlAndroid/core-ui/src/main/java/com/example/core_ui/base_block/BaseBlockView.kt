package com.example.core_ui.base_block

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.core_architecture.base_fragment.recycler_view.CustomAdapter
import com.example.core_ui.databinding.ItemBaseBlockBinding


class BaseBlockView : ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val binding: ItemBaseBlockBinding = ItemBaseBlockBinding
        .inflate(LayoutInflater.from(context), this, true)

    private val itemTitle = binding.itemTitle
    private val itemRecyclerView = binding.itemRecyclerView
    private val itemExpandable = binding.itemExpandable

    private lateinit var adapter: CustomAdapter

    init {
        layoutParams = LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        itemRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    fun setBaseBlock(item: BaseBlockItem) {
        item.isExpandable?.let {
            itemExpandable.isVisible = it
            itemExpandable.setOnClickListener {
                itemExpandable.rotation = 180.0f
                if(itemRecyclerView.visibility == LinearLayout.VISIBLE) {
                    itemRecyclerView.visibility = GONE
                } else {
                    itemExpandable.rotation = 0.0f
                    itemRecyclerView.visibility = VISIBLE
                }
            }
        }

        item.isVertical?.let {
            if(it == false) {
                itemRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            }
        }

        item.blockTitle?.let {
            itemTitle.text = it
        }

        item.dataSet?.let {
            adapter = CustomAdapter(
                dataSet = it,
                callback = object : CustomAdapter.Callback {
                    override fun setOnItemClicked() {
                        item.setOnClickListener()
                    }
                }
            )
            itemRecyclerView.adapter = adapter
        }
    }
}