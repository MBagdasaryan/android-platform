package com.example.core_ui.base_view_pager

import android.content.Context
import com.example.core_ui.base_button.BaseButtonView

fun baseViewPager(
    context: Context,
    item: BaseViewPagerItem
): BaseViewPagerView {
    val view = BaseViewPagerView(context)
    view.setBaseViewPager(item)

    return view
}
