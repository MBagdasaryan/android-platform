package com.example.core_ui.simple_block

import androidx.constraintlayout.widget.ConstraintLayout

data class SimpleBlockItem (
    val blockTitle: String? = "",
    val isExpandable: Boolean? = false,
    val isVertical: Boolean? = true,
    val dataSet: Array<ConstraintLayout>? = null,
    val setOnClickListener: ()->Any = {}
)