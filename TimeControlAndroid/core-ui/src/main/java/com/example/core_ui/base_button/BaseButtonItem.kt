package com.example.core_ui.base_button

data class BaseButtonItem (
    val btnText: String? = "",
    val setOnClickListener: ()->Any? = {}
)