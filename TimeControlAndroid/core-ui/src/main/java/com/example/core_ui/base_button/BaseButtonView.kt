package com.example.core_ui.base_button

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.core_ui.R
import com.example.core_ui._tools.dp
import com.example.core_ui.databinding.ItemBaseButtonBinding

class BaseButtonView : ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val binding: ItemBaseButtonBinding = ItemBaseButtonBinding
        .inflate(LayoutInflater.from(context), this, true)

    private val baseButton = binding.itemBaseButton

    init {
        layoutParams = LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            70.dp // LinearLayout.LayoutParams.WRAP_CONTENT
        )
    }

    fun setBaseButton(item: BaseButtonItem) {
        item.btnText?.let {
            baseButton.text = it
        }

        baseButton.background = ContextCompat.getDrawable(context, R.drawable.ic_base_shape)

        baseButton.setOnClickListener {
            item.setOnClickListener()
        }
    }
}