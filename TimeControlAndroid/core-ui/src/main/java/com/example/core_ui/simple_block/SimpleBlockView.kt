package com.example.core_ui.simple_block

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import com.example.core_architecture.base_fragment.recycler_view.CustomAdapter
import com.example.core_ui.databinding.ItemSimpleBlockBinding

class SimpleBlockView : ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val binding: ItemSimpleBlockBinding = ItemSimpleBlockBinding
        .inflate(LayoutInflater.from(context), this, true)

    private val itemTitle = binding.itemSimpleTitle
    private val itemLayoutView = binding.itemSimpleLayoutView
    private val itemExpandable = binding.itemSimpleExpandable

    init {
        layoutParams = LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
    }

    fun setSimpleBlock(item: SimpleBlockItem) {
        item.isExpandable?.let {
            itemExpandable.isVisible = it
            itemExpandable.setOnClickListener {
                itemExpandable.rotation = 180.0f
                if(itemLayoutView.visibility == LinearLayout.VISIBLE) {
                    itemLayoutView.visibility = GONE
                } else {
                    itemExpandable.rotation = 0.0f
                    itemLayoutView.visibility = VISIBLE
                }
            }
        }

        item.blockTitle?.let {
            itemTitle.text = it
        }

        item.dataSet?.let {
            it.map {
                itemLayoutView.addView(it)
            }
        }
    }
}