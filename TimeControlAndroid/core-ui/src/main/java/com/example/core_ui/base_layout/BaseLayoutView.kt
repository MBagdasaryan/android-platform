package com.example.core_ui.base_layout

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.core_ui.R
import com.example.core_ui.databinding.ItemBaseLayoutBinding

class BaseLayoutView : ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val binding: ItemBaseLayoutBinding = ItemBaseLayoutBinding.inflate(LayoutInflater.from(context), this, false)

    private val itemBaseLayout = binding.itemBaseLayout

    init {
        layoutParams = LayoutParams(
            LayoutParams.WRAP_CONTENT,
            LayoutParams.WRAP_CONTENT
        )
        itemBaseLayout.background = ContextCompat.getDrawable(context, R.drawable.ic_base_shape)
    }
}