package com.example.core_ui.simple_block

import android.content.Context

fun simpleBlock(
    context: Context,
    item: SimpleBlockItem
): SimpleBlockView {
    val view = SimpleBlockView(context)
    view.setSimpleBlock(item)

    return view
}