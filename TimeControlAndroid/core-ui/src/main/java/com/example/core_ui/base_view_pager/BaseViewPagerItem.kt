package com.example.core_ui.base_view_pager

import androidx.constraintlayout.widget.ConstraintLayout

data class BaseViewPagerItem (
    val dataSet: Array<ConstraintLayout>? = null,
)