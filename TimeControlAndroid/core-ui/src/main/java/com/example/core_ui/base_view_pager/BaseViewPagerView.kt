package com.example.core_ui.base_view_pager

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2
import com.example.core_architecture.base_fragment.recycler_view.ViewPagerAdapter
import com.example.core_ui.R
import com.example.core_ui.base_button.BaseButtonItem
import com.example.core_ui.base_button.baseButton
import com.example.core_ui.databinding.ItemBaseViewPagerBinding
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class BaseViewPagerView: ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private lateinit var adapter: ViewPagerAdapter

    private val binding: ItemBaseViewPagerBinding = ItemBaseViewPagerBinding
        .inflate(LayoutInflater.from(context), this, true)
    private val pager = binding.pager
    private val tabLayout = binding.tabLayout
//    private val indicators = binding.indicators

    init {
        layoutParams = LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
    }

    fun setBaseViewPager(item: BaseViewPagerItem) {
        item.dataSet?.let {
            val data = listOf(it.last()).plus(it).plus(listOf(it.first()))

            adapter = ViewPagerAdapter(
                dataSet = Array(1){
                    item.dataSet[item.dataSet.size-1] // baseButton(context, item = BaseButtonItem()) as ConstraintLayout
                }.plus(item.dataSet).plus(
                    arrayOf(item.dataSet[0]) // Array(1){baseButton(context, item = BaseButtonItem()) as ConstraintLayout}
                ),
                callback = object : ViewPagerAdapter.Callback {
                    override fun setOnItemClicked() {
                        //
                    }
                }
            )
            pager.adapter = adapter

            pager.setCurrentItem(1, false)
            pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    tabLayout.getTabAt(0)?.let {
                        (tabLayout.getTabAt(0)?.view as LinearLayout).visibility = View.GONE
                    }

                    tabLayout.getTabAt(adapter.itemCount-1)?.let {
                        (tabLayout.getTabAt(adapter.itemCount-1)?.view as LinearLayout).visibility = View.GONE
                    }

                    println("Position -> ${position}\n")
                    if(positionOffsetPixels == 0 && position == adapter.itemCount-1) {
                        pager.setCurrentItem(1, false)
                    }else if(positionOffsetPixels == 0 && position == 0) {
                        pager.setCurrentItem(adapter.itemCount-2, false)
                    }
                }
            })

//            tabLayout.getTabAt(0)?.icon?.setVisible(false, false)

            TabLayoutMediator(
                tabLayout,
                pager
            ) { tab, position ->
                if(position == 0) {
                    tab.setCustomView(null)
                    tab.tabLabelVisibility = TabLayout.TAB_LABEL_VISIBILITY_UNLABELED
                }
            }.attach()
        }
    }

    fun LinearLayout.setImageColor( index: Int, dataSize: Int) {
        this.removeAllViews()
        for(i in 0 until dataSize) {
            val imageParams = LayoutParams(30, 30)
            imageParams.setMargins(30, 0, 30, 0)
            val x = ImageView(context)
            x.layoutParams = imageParams
            x.minimumWidth = 20
            x.minimumHeight = 20
            if(index == i) {
                x.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_selected_dot))
            } else {
                x.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_default_dot))
            }
            this.addView(x)
        }
    }
}