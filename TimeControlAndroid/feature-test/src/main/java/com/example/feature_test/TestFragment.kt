package com.example.feature_test

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.core_architecture.base_fragment.BaseRecyclerFragment
import com.example.core_architecture.base_fragment.replace.ReplaceCreator
import com.example.core_ui.base_block.BaseBlockItem
import com.example.core_ui.base_block.baseBlock
import com.example.core_ui.base_button.BaseButtonItem
import com.example.core_ui.base_button.baseButton
import com.example.core_ui.base_view_pager.BaseViewPagerItem
import com.example.core_ui.base_view_pager.baseViewPager
import com.example.core_ui.simple_block.SimpleBlockItem
import com.example.core_ui.simple_block.simpleBlock
import org.koin.android.ext.android.inject

class TestFragment: BaseRecyclerFragment(
    showBackButton = false,
    toolbarTitle = "Home"
), TestContract.View {
    private val presenter: TestPresenter by inject()

    override var BLOCK_SIZE = 7

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
    }

    override fun initViews() {
        recyclerBlock[0] = simpleBlock(
            requireContext(),
            SimpleBlockItem(
                blockTitle = "First block",
                isExpandable = true,
                dataSet = arrayOf(
                    baseButton(
                        requireContext(),
                        item = BaseButtonItem(
                            btnText = "Some button",
                            setOnClickListener = {
                                presenter.clickOnButton()
                                Toast.makeText(requireContext(), "Hello text", Toast.LENGTH_SHORT).show()
                            }
                        )
                    ),
                    baseButton(
                        requireContext(),
                        item = BaseButtonItem(
                            btnText = "Second button",
                            setOnClickListener = {
                                // Do nothing...
                            }
                        )
                    )
                )
            )
        )

        recyclerBlock[1] = baseBlock(
            requireContext(),
            BaseBlockItem(
                blockTitle = "Second block",
                isVertical = false,
                dataSet = arrayOf(
                    baseButton(
                        requireContext(),
                        BaseButtonItem(
                            btnText = "Vertical 1",
                            setOnClickListener = {

                            }
                        )
                    ),
                    baseButton(
                        requireContext(),
                        BaseButtonItem(
                            btnText = "Vertical 2",
                            setOnClickListener = {

                            }
                        )
                    ),
                    baseButton(
                        requireContext(),
                        BaseButtonItem(
                            btnText = "Vertical 3",
                            setOnClickListener = {

                            }
                        )
                    )
                )
            )
        )

        recyclerBlock[2] = simpleBlock(
            requireContext(),
            SimpleBlockItem(
                blockTitle = "ViewPager block",
                isExpandable = true,
                dataSet = arrayOf(
                    baseViewPager(
                        requireContext(),
                        item = BaseViewPagerItem(
                            dataSet = arrayOf(
                                baseButton(
                                    requireContext(),
                                    BaseButtonItem(
                                        btnText = "Vertical 1",
                                        setOnClickListener = {

                                        }
                                    )
                                ),
                                baseButton(
                                    requireContext(),
                                    BaseButtonItem(
                                        btnText = "Vertical 2",
                                        setOnClickListener = {

                                        }
                                    )
                                ),
                                baseButton(
                                    requireContext(),
                                    BaseButtonItem(
                                        btnText = "Vertical 3",
                                        setOnClickListener = {

                                        }
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )

        recyclerBlock[3] = simpleBlock(
            requireContext(),
            SimpleBlockItem(
                blockTitle = "First block",
                isExpandable = true,
                dataSet = arrayOf(
                    baseButton(
                        requireContext(),
                        item = BaseButtonItem(
                            btnText = "Some button",
                            setOnClickListener = {
                                presenter.clickOnButton()
                                Toast.makeText(requireContext(), "Hello text", Toast.LENGTH_SHORT).show()
                            }
                        )
                    )
                )
            )
        )

        recyclerBlock[4] = simpleBlock(
            requireContext(),
            SimpleBlockItem(
                blockTitle = "First block",
                isExpandable = true,
                dataSet = arrayOf(
                    baseButton(
                        requireContext(),
                        item = BaseButtonItem(
                            btnText = "Some button",
                            setOnClickListener = {
                                presenter.clickOnButton()
                                Toast.makeText(requireContext(), "Hello text", Toast.LENGTH_SHORT).show()
                            }
                        )
                    )
                )
            )
        )

        recyclerBlock[5] = simpleBlock(
            requireContext(),
            SimpleBlockItem(
                blockTitle = "First block",
                isExpandable = true,
                dataSet = arrayOf(
                    baseButton(
                        requireContext(),
                        item = BaseButtonItem(
                            btnText = "Some button",
                            setOnClickListener = {
                                presenter.clickOnButton()
                                Toast.makeText(requireContext(), "Hello text", Toast.LENGTH_SHORT).show()
                            }
                        )
                    )
                )
            )
        )

        recyclerBlock[6] = simpleBlock(
            requireContext(),
            SimpleBlockItem(
                blockTitle = "First block",
                isExpandable = true,
                dataSet = arrayOf(
                    baseButton(
                        requireContext(),
                        item = BaseButtonItem(
                            btnText = "Some button",
                            setOnClickListener = {
                                presenter.clickOnButton()
                                Toast.makeText(requireContext(), "Hello text", Toast.LENGTH_SHORT).show()
                            }
                        )
                    )
                )
            )
        )
    }

    override fun showButton() {
        ReplaceCreator(requireActivity())
            .setToFragment(SecondFragment())
            .replaceFragment()
    }
}