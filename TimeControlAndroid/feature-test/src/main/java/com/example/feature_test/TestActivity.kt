package com.example.feature_test

import com.example.core_architecture.base_fragment.BaseActivity
import com.example.core_architecture.base_fragment.BaseFragment
import com.example.core_architecture.base_fragment.BaseRecyclerFragment

class TestActivity: BaseActivity() {
    override val getFragment: BaseRecyclerFragment = TestFragment()
    override fun setBottomNavigation() {
        addBottomNavigation(
            menu = R.menu.bottom_nav_menu_test_app,
            fragmentsList = arrayOf(
                TestFragment(),
                SecondFragment(),
                ThirdFragment()
            ),
            idList = arrayOf(
                R.id.first_bottom_menu,
                R.id.second_bottom_menu,
                R.id.third_bottom_menu
            ),
            setOnClickListener = {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.activity_main_layout, it)
                    .commit()
            }
        )
    }
}