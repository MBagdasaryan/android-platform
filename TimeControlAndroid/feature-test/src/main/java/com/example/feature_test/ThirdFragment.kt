package com.example.feature_test

import android.widget.Button
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.core_architecture.base_fragment.BaseFragment
import com.example.core_architecture.base_fragment.BaseRecyclerFragment
import com.example.core_architecture.base_fragment.replace.ReplaceCreator
import com.example.core_architecture.base_fragment.replace.ReplaceUtils
import com.example.core_ui.base_block.BaseBlockItem
import com.example.core_ui.base_block.baseBlock
import com.example.core_ui.base_button.BaseButtonItem
import com.example.core_ui.base_button.baseButton
import com.example.core_ui.simple_block.SimpleBlockItem
import com.example.core_ui.simple_block.simpleBlock
import com.example.feature_second_test.SecondTestActivity

class ThirdFragment : BaseRecyclerFragment(
    toolbarTitle = "Third fragment"
) {
    override var BLOCK_SIZE = 1

    override fun initViews() {
        recyclerBlock[0] = simpleBlock(
            requireContext(),
            SimpleBlockItem(
                blockTitle = "First block",
                dataSet = arrayOf(
                    baseButton(
                        requireContext(),
                        BaseButtonItem(
                            btnText = "Third fragment",
                            setOnClickListener = {
                                ReplaceCreator(requireActivity())
                                    .setToActivity(SecondTestActivity())
                                    .replaceActivity()
                            }
                        )
                    )
                )
            )
        )
    }
}