package com.example.feature_test

import com.example.core_architecture.base_fragment.MVP.BaseView
import com.example.core_architecture.base_fragment.MVP.MvpPresenter

class TestContract {
    interface View: BaseView {
        fun showButton()
    }

    interface Presenter: MvpPresenter<View> {
        fun clickOnButton()
    }
}
