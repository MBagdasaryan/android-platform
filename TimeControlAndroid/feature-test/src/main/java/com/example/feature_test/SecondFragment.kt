package com.example.feature_test

import com.example.core_architecture.base_fragment.BaseRecyclerFragment
import com.example.core_architecture.base_fragment.replace.ReplaceCreator
import com.example.core_ui.base_button.BaseButtonItem
import com.example.core_ui.base_button.baseButton
import com.example.core_ui.simple_block.SimpleBlockItem
import com.example.core_ui.simple_block.simpleBlock

class SecondFragment: BaseRecyclerFragment(
    toolbarTitle = "Second fragment"
) {
    override var BLOCK_SIZE = 1

    override fun initViews() {
        recyclerBlock[0] = simpleBlock(
            requireContext(),
            SimpleBlockItem(
                blockTitle = "First block",
                isExpandable = true,
                dataSet = arrayOf(
                    baseButton(
                        requireContext(),
                        item = BaseButtonItem(
                            btnText = "Second fragment",
                            setOnClickListener = {
                                ReplaceCreator(requireActivity())
                                    .setToFragment(ThirdFragment())
                                    .replaceFragment()
                            }
                        )
                    )
                )
            )
        )
    }
}