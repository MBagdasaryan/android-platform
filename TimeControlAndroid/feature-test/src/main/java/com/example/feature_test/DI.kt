package com.example.feature_test

import org.koin.dsl.module

val dataModule = module {
    factory { TestPresenter() }
}
