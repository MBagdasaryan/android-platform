package com.example.feature_test

import com.example.core_architecture.base_fragment.MVP.BasePresenter

class TestPresenter():
    BasePresenter<TestContract.View>(),
    TestContract.Presenter {

    override fun clickOnButton() {
//        view?.showLoader()
//        Thread.sleep(4000)
//        view?.hideLoader()

        view?.showButton()
    }
}
