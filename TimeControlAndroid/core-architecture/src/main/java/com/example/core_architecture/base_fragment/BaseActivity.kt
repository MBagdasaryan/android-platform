package com.example.core_architecture.base_fragment

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import com.example.core_architecture.R
import com.example.core_architecture.databinding.BaseLayoutBinding
import com.google.android.material.bottomnavigation.BottomNavigationView


abstract class BaseActivity(): AppCompatActivity() {
    abstract val getFragment: BaseRecyclerFragment
    private lateinit var binding: BaseLayoutBinding
    private lateinit var tbBack: AppCompatButton
    private lateinit var bottomNavigation: BottomNavigationView
    lateinit var mainLayout: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_layout)

        binding = BaseLayoutBinding.inflate(layoutInflater)
        tbBack = findViewById(R.id.tb_back)
        mainLayout = findViewById(R.id.container)
        bottomNavigation = findViewById(R.id.item_bottom_navigation)

        bottomNavigation.visibility = View.GONE

        supportActionBar?.let {
            it.hide()
        }
        onBackClicked()

        val mFragmentManager = supportFragmentManager
        val fragmentTransaction = mFragmentManager.beginTransaction()

        fragmentTransaction
            .replace(R.id.activity_main_layout, getFragment)
            .commit()

        setBottomNavigation()
    }

    open fun setBottomNavigation(){}

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            android.R.id.home -> {
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun addBottomNavigation(
        menu: Int,
        fragmentsList: Array<Fragment>,
        idList: Array<Int>,
        setOnClickListener: (fragment: Fragment)->Unit = {}
    ) {
        bottomNavigation.visibility = View.VISIBLE
        bottomNavigation.inflateMenu(menu)
        bottomNavigation.setOnItemSelectedListener { item ->
            for(index in idList.indices) {
                if(item.itemId == idList[index]) {
                    setOnClickListener(fragmentsList[index])
                    break
                }
            }

            true
        }
    }

    fun onBackClicked() {
        tbBack.setOnClickListener {
            this.onBackPressed()
        }
    }
}
