package com.example.core_architecture.base_fragment.MVP

interface MvpPresenter<T: Any> {
    fun attachView(mView: T) {}
}
