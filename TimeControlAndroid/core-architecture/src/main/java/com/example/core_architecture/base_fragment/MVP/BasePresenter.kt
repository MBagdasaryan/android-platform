package com.example.core_architecture.base_fragment.MVP

abstract class BasePresenter<T: BaseView>: MvpPresenter<T> {
    var view: T? = null

    override fun attachView(mView: T) {
        view = mView
    }
}
