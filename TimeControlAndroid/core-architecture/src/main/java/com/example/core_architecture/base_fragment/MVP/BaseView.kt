package com.example.core_architecture.base_fragment.MVP

interface BaseView {
    fun showLoader()
    fun hideLoader()
}
