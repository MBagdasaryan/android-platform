package com.example.core_architecture.base_fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.core_architecture.R
import android.content.Intent
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.core_ui.base_block.BaseBlockView
import com.example.core_ui.base_block.baseBlock


abstract class BaseFragment(
    val showBackButton: Boolean? = true,
    val toolbarTitle: String? = null
) : Fragment() {
    abstract var BLOCK_SIZE: Int
    lateinit var layoutView: LinearLayout
    lateinit var recyclerBlock: Array<BaseBlockView?>
    private lateinit var tbBack: AppCompatButton
    private lateinit var tbTitle: AppCompatTextView
    val mainLayout = R.layout.base_layout

    override fun getContext(): Context? {
        return activity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.base_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        println("onViewCreated")
        super.onViewCreated(view, savedInstanceState)
        layoutView = view.findViewById(R.id.base_container)
        tbBack = requireActivity().findViewById(R.id.tb_back)
        tbTitle = requireActivity().findViewById(R.id.tb_title)

        recyclerBlock = Array(BLOCK_SIZE) {null}

        initViews()
        setRecycler()

        showBackButton?.let {
            tbBack.visibility = if(showBackButton) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        toolbarTitle?.let {
            tbTitle.text = it
        }
    }

    abstract fun initViews()

    private fun setRecycler() {
        for(index in 0 until BLOCK_SIZE) {
            layoutView.addView(recyclerBlock[index])
        }
    }
}