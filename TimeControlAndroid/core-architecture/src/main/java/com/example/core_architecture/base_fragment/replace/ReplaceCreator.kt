package com.example.core_architecture.base_fragment.replace

import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.core_architecture.R
import com.example.core_architecture.base_fragment.BaseActivity
import com.example.core_architecture.base_fragment.BaseFragment

class ReplaceCreator(val activity: FragmentActivity) {
    private lateinit var setToActivity: BaseActivity
    private lateinit var setToFragment: Fragment
    private var intent = Intent()

    fun setToActivity(value: BaseActivity): ReplaceCreator {
        setToActivity = value
        return this
    }

    fun setExtra(key: String, value: Any): ReplaceCreator {
        when(value) {
            is Int -> intent.putExtra(key, value)
            is String -> intent.putExtra(key, value)
        }
        return this
    }

    fun replaceActivity(): ReplaceCreator {
        intent = Intent(activity, setToActivity::class.java)
        activity.startActivity(intent)
        return this
    }

    fun setToFragment(value: Fragment): ReplaceCreator {
        setToFragment = value
        return this
    }

    fun replaceFragment(): ReplaceCreator {
        activity.supportFragmentManager
            .beginTransaction()
            .replace(R.id.activity_main_layout, setToFragment)
            .addToBackStack(null)
            .commit()
        return this
    }
}