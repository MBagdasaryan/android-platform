package com.example.core_architecture.base_fragment.replace

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.FragmentActivity
import com.example.core_architecture.R
import com.example.core_architecture.base_fragment.BaseActivity
import com.example.core_architecture.base_fragment.BaseFragment

class ReplaceUtils(val activity: FragmentActivity) {
    fun replaceFragment(toFragment: BaseFragment) {
        activity.supportFragmentManager
            .beginTransaction()
            .replace(R.id.main_layout, toFragment)
            .addToBackStack(null)
            .commit()
    }

    fun replaceActivity(toActivity: Activity) {
        val intent = Intent(activity, toActivity::class.java)
        activity.startActivity(intent)
    }
}