package com.example.core_architecture.base_fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.core_architecture.R
import com.example.core_architecture.base_fragment.MVP.BaseView
import com.example.core_architecture.base_fragment.recycler_view.CustomAdapter

abstract class BaseRecyclerFragment(
    val showBackButton: Boolean? = true,
    val toolbarTitle: String? = null
) : Fragment(), BaseView {
    abstract var BLOCK_SIZE: Int
    lateinit var layoutView: ConstraintLayout
    lateinit var progressBar: ProgressBar

    lateinit var recyclerBlock: Array<ConstraintLayout?>
    private lateinit var tbBack: AppCompatButton
    private lateinit var tbTitle: AppCompatTextView
    private lateinit var recycler: RecyclerView

    override fun getContext(): Context? {
        return activity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.base_recycler_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        layoutView = view.findViewById(R.id.base_main_recycler_layout)
        progressBar = view.findViewById(R.id.progressBar)

        tbBack = requireActivity().findViewById(R.id.tb_back)
        tbTitle = requireActivity().findViewById(R.id.tb_title)

        recyclerBlock = Array(BLOCK_SIZE){null}

        initViews()
        setRecycler()

        showBackButton?.let {
            tbBack.visibility = if(showBackButton) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        toolbarTitle?.let {
            tbTitle.text = it
        }
    }

    abstract fun initViews()

    private fun setRecycler() {
        val mRecycler = RecyclerView(requireContext())
        mRecycler.layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )

        mRecycler.layoutManager = LinearLayoutManager(requireContext())
        mRecycler.adapter = CustomAdapter(
            dataSet = recyclerBlock,
            callback = object : CustomAdapter.Callback {
                override fun setOnItemClicked() {
                    // Do nothing...
                }
            }
        )

        layoutView.addView(mRecycler)
    }

    override fun showLoader() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        progressBar.visibility = View.GONE
    }
}
